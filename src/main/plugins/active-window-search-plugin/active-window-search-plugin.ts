import { SearchPlugin } from "../../search-plugin";
import { SearchResultItem } from "../../../common/search-result-item";
import { UserConfigOptions } from "../../../common/config/user-config-options";
import { PluginType } from "../../plugin-type";
//import { ApplicationSearchOptions } from "../../../common/config/application-search-options";
import { defaultWindowsAppIcon } from "../../../common/icon/default-icons";

import  windowManager  = require("node-window-manager");
import Window = windowManager.Window;

export class ActiveWindowSearchPlugin implements SearchPlugin {
    public readonly pluginType = PluginType.ActiveWindowSearchPlugin;    
    private applications: Window[];

    constructor() {
        this.applications = [];
    }

    getAll(): Promise<SearchResultItem[]> {
        return Promise.resolve(this.applications.map((app) => this.createSearchResultItemFromWindow(app)));
    }

    refreshIndex(): Promise<void> {
        console.log('fooo');
        return new Promise((resolve,reject) => {
            this.searchActiveWindows()
                .then((result) => {
                    this.applications = result;
                    resolve()
                })
                .catch((err) => reject(err))
        })
       
    }

    clearCache(): Promise<void> {
        throw new Error("Method not implemented.");
    }

    isEnabled(): boolean {
        return true;//this.config.enabled;
    }

    execute(searchResultItem: SearchResultItem, privileged: boolean = false): Promise<void> {
        return new Promise((resolve,reject) => {
            this.bringToTop(parseInt(searchResultItem.executionArgument))
                .then(() => resolve())
                .catch(() => reject());
        })
    }

    updateConfig(config: UserConfigOptions): Promise<void> {
        return new Promise((resolve) => resolve());
    }

    private createSearchResultItemFromWindow(application: Window): SearchResultItem {
        return {
            description: '',
            executionArgument: application.id + '',
            hideMainWindowAfterExecution: true,
            icon: defaultWindowsAppIcon,
            name: application.getTitle(),
            originPluginType: this.pluginType,
            searchable: [application.getTitle()],
        };
    }

    private searchActiveWindows(): Promise<Window[]> {
        return new Promise((resolve, reject) => {
            let result = windowManager.windowManager.getWindows()
                .filter((window) => (window.isVisible() && window.getTitle().trim() !== '' && window.getBounds().x != 0))
            if(result){
                resolve(result);
            }else {
                reject();
            }
        });
    }

    private bringToTop(id: number): Promise<void> {
        return new Promise((resolve, reject) => {
            let windows = this.applications
                .filter((window) => window.id === id)
            if(windows && windows.length == 1){
                windows[0].maximize();
                windows[0].bringToTop();
                resolve()
            } else {
                reject()
            }
        });
    }
}
